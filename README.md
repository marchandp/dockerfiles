# dockerfiles

Repository of custom Dockerfiles used for R work. All the images build on the [Rocker images](https://www.rocker-project.org/images/).

## `r3.4.4`

- used for dev work of ToolsISO13399 package
- has R version 3.4.4, and required packages
- 
## `r3.6.0`

- used for dev work of ToolsISO13399 package
- has R version 3.6.0, and required packages
- based on Rstudio image
- to use R studio, see https://github.com/rocker-org/rocker-versioned/tree/master/rstudio
- docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
- docker login -u marchandp -p yourpasswordhere registry.gitlab.com
- docker run --rm -p 8787:8787 -e PASSWORD=yourpasswordhere  registry.gitlab.com/marchandp/dockerfiles/rdev:3.6.0

docker run --rm \\

    -p 127.0.0.1:8787:8787 \\

    -v $(pwd):/home/rstudio \\
    
    -e DISABLE_AUTH=true \\
    
    registry.gitlab.com/marchandp/dockerfiles/rdev:3.6.0
    
- to launch Rstudio:
- 
localhost:8787

## `rhub`
- to be tested TBD
- used for checking packages with rhub
- has current R base and rhub
- requires a `resources\validated_emails.csv` file to build
- to get `validated_emails.csv`, run `rhub::validate_email()` and it should save that file into `rappdirs::user_data_dir("rhub", "rhub")`
gitlab-ci.yml
see: https://codebabel.com/ci-gitlabci-docker/
